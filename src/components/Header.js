import React from 'react'
import { Link } from 'react-router-dom'

export const Header = () => {
    return (
        <nav className='nav'>
            <div className='nav-inner'>
                <h1>Expenses Tracker</h1>
                <ul className="nav-items">
                    <Link to='/expenses'>
                        <li className='nav-item'>Home</li>
                    </Link>
                    <Link to='/about'>
                        <li className='nav-item'>About</li>
                    </Link>
                </ul>
            </div>
        </nav>
    )
}
