import React, { useContext } from 'react'
import { GlobalStateContext } from '../context/GlobalState'
import Transaction from './Transaction'

function TransactionList() {
    const { transactions } = useContext(GlobalStateContext)

    return (
        <div>
            <h3>History</h3>
            <ul id="list" className="list">
                {transactions.map(transaction => (
                    <Transaction key={transaction.id} transaction={transaction} />
                ))}
            </ul>
        </div>
    )
}

export default TransactionList
