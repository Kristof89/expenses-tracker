import React from 'react';
import './App.css';
import { Header } from './components/Header';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import About from './pages/About'
import Home from './pages/Home'

import { GlobalStateProvider } from './context/GlobalState';

function App() {
  return (
    <Router>
      <GlobalStateProvider>
        <Header />
        <div className="container">
          <Switch>
            <Route path='/expenses' exact component={Home} />
            <Route path='/about' component={About} />
          </Switch>
        </div>
      </GlobalStateProvider>
    </Router>
  );
}

export default App;
