import React from 'react'

function About() {
    return (
        <div>
            <h2>Project</h2>
            <p className='about'>
                EXPENSES TRACKER was built based on <a href='https://www.youtube.com/channel/UC29ju8bIPH5as8OGnQzwJyA' rel="noopener noreferrer" target='_blank'>Traversy Media</a> and <a href='https://www.youtube.com/watch?v=Law7wfdg_ls' rel="noopener noreferrer" target='_blank'> Ed Dev</a> video tutorials. 
            </p>
            <p className='about'>
                It gave me a better understanding of React Hooks functionality especially useState and useContext hooks.
                Improved my general understanding of React JS.
            </p>
            <p className='about'>
                As a future improvement I might add database like MongoDB to store all expenses.
            </p>
        </div>
    )
}

export default About
